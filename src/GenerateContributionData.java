import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class GenerateContributionData {
	//Constants
	private static int bufferSize = 8 * 1024;
	private static String inputFile;
	private static String outputByZip;
	private static String outputByDate;
	private static BufferedWriter bwforZipFile = null;
	private static BufferedWriter bwforDateFile = null;
	
	//Input file fields
	private static String cmteId;       
	private static String zipCode;       
	private static String transactionDate;
	private static String transactionAmt;
	private static String otherId;
	
	//Storage
	private static HashMap<String,ContributionByZip> zipcodeContributionDataMap = new HashMap<>();
	private static HashMap<String, ContributionByDate> dateContributionDataMap  = new HashMap<>();
	
	
	//Sorted output 
	private static Comparator<ContributionByDate> queueComparator = new ReciverandDateComparator();
	private static PriorityQueue<ContributionByDate> priorityQueue;
	
	
	/*
	 * Processes the current record from the input file, 
	 * 1) Parses the input file to extract appropriate input fields,
	 * 2) The input field is checked for validitity and if it's valid further processing takes place.
	 * 3) Concetanante CMTE_ID and Zip-code and check if the value already exists in the input file - If it does then update
	 * 4) If it's a completely new record then create a new entry.
	 */
	public static void processRecord(String currentRecord) {
		
		String[] currentRecordSplit = currentRecord.split("\\|");
		
		cmteId           = currentRecordSplit[0];
		
		if(currentRecordSplit[10].length() > 4)
			zipCode      = currentRecordSplit[10].substring(0, 5);
		else
			zipCode      = "";
		
		if(currentRecordSplit[13].length() == 8)
			transactionDate  = currentRecordSplit[13];
		else 
			transactionDate  = "";
		
		transactionAmt   = currentRecordSplit[14];
		otherId          = currentRecordSplit[15];
		
	}
	
	/*
	 *-------------------------- Contribution by Zip Processing --------------------------------------------
	 */
	
	/*
	 * Validation function for zipcode based processing
	 * Mark the record as Valid/Invalid - Check for essential fields of the processing medianvals_by_zip
	 */
	
	public static boolean checkValidityforZipcodeProcessing() {
		
		if(cmteId == null || cmteId.isEmpty() || zipCode == null || zipCode.isEmpty() || 
		  !(zipCode.matches("\\d+")) || transactionAmt == null || transactionAmt.isEmpty() || !otherId.isEmpty())
			return false;
		
		return true;
	}
	
	/*
	 * Each record as it's read from the buffer, We have to two options to deal with it.
	 * 1) Create a new Object if the record is completely new for the combination of Reciever(CMTE_ID)/ZipCode.
	 * 2) Get the existing Object that holds the value for given Reciever(CMTE_ID)/ZipCode and 
	 * update the  Median amount, No of Transactions and Total amount. 
	 */
	
	public static void processRecordForContributionByZip(String cmteID, String zipCode, String transactionAmt) {
		
		//Check if already a valid object exists pertaining to same CMTE_ID and zipCode - If yes - Update the object.
		StringBuilder cmteIdZip = new StringBuilder("").append(cmteID).append(zipCode);
		String cmteIdZipStr = cmteIdZip.toString();
		long formattedtransactionAmt = Long.parseLong(transactionAmt);
		
		
		if(zipcodeContributionDataMap.containsKey(cmteIdZipStr)) {
			ContributionByZip currentContribution = zipcodeContributionDataMap.get(cmteIdZipStr);
			currentContribution.updateTotalAmount(formattedtransactionAmt);
		}else {
			zipcodeContributionDataMap.put(cmteIdZipStr, new ContributionByZip(cmteID, zipCode, formattedtransactionAmt));
		}
		
		//Output the record into the file - Synonymous to updating the dashboard
		writeContribution(zipcodeContributionDataMap.get(cmteIdZipStr), bwforZipFile);
	}
	
	/*
	 * Helper function to write files into output file - medianvals_by_zip
	 * Output file: medianvals_by_zip
	 */
	
	public static void writeContribution(ContributionByZip currentZipRecord, BufferedWriter bw) {
			
		try {
				bw.write(currentZipRecord.getReciever()+"|"+currentZipRecord.getZipcode()+"|"+
						currentZipRecord.getMedianAmount()+"|" +currentZipRecord.getNoOfTransacations()+
						"|"+currentZipRecord.getTotalAmount() +"\n");
				
			} catch (IOException e) {
				System.out.println("Error while writing into output file - medianvals_by_zip");
				e.printStackTrace();
			}
		
	}
	
	
	
	/*
	 *-------------------------- Contribution by Date Processing --------------------------------------------
	 */
	
	/*
	 * Validation function for Date-based processing
	 * Mark the record as Valid/Invalid - Check for essential fields of the processing medianvals_by_date
	 */
	
	public static boolean checkValidityforDateProcessing() {
		
		if(cmteId == null || cmteId.isEmpty() || transactionDate == null || transactionDate.isEmpty() || 
			(!validDate(transactionDate)) || transactionAmt == null || transactionAmt.isEmpty() || !otherId.isEmpty())
			return false;
		
		return true;
	}
	
	public static boolean validDate(String transactionDate) {
		if(!(transactionDate.matches("\\d+")))
				return false;
		else {
			int month = Integer.parseInt(transactionDate.substring(0,2));
			int days	  = Integer.parseInt(transactionDate.substring(2,4));
			int year	  = Integer.parseInt(transactionDate.substring(4,8));
			//Basic validation
			if(month < 1 || month > 12 || days < 1 || days > 31 || (year < 1900 || year >= 2018))
				return false;
			//Even months
			else if((month == 4 || month == 6 || month == 9 || month == 11) && days > 30)
				return false;
			//February validation
			else if( ((year % 4 != 0) && (month == 2 && days >= 29)) || 
				    ( ((year%4 == 0 && year%100 != 0) || (year%400 == 0)) && month == 2 && (days > 29))
				    )
				return false;
			else
				return true;
			
		}
	}
	
	/*
	 * Each record as it's read from the buffer, We have to two options to deal with it.
	 * 1) Create a new Object if the record is completely new for the combination of Reciever(CMTE_ID)/Transaction Date.
	 * 2) Get the existing Object that holds the value for given Reciever(CMTE_ID)/Transaction Date and 
	 *  update the Median amount, No of Transactions and Total amount. 
	 */
	
	public static void processRecordForContributionByDate(String cmteID, String transactionDate, String transactionAmt) {
		
		StringBuilder cmteIdDate = new StringBuilder("").append(cmteID).append(transactionDate);
		String cmteIdDateStr = cmteIdDate.toString();
		long formattedtransactionAmt = Long.parseLong(transactionAmt);
		
		
		if(dateContributionDataMap.containsKey(cmteIdDateStr)) {
			ContributionByDate currentContribution = dateContributionDataMap.get(cmteIdDateStr);
			currentContribution.updateTotalAmount(formattedtransactionAmt);
		}else {
			dateContributionDataMap.put(cmteIdDateStr, new ContributionByDate(cmteID, transactionDate, formattedtransactionAmt));
		}
		
	}
	
	
	/*
	 * Add the data into priority queue - Optimally adds the records in sorted order
	 * - The custom Comparator has been provided in the RecieverDateComparator 
	 */
	
	public static void addIntoPriorityQueue() {
		
		for(Map.Entry<String, ContributionByDate> entry: dateContributionDataMap.entrySet()) {
			priorityQueue.add(entry.getValue());
		}
		
	}
	
	public static void writePriorityQueue() {
		
		while(priorityQueue.size() != 0) {
			ContributionByDate currentContributionByDate = priorityQueue.poll();
			writeContributionByDate(currentContributionByDate, bwforDateFile);
		}
	}
	
	public static void writeContributionByDate(ContributionByDate currentContributionDate, BufferedWriter bw) {
		
		
		try {
				bw.write(currentContributionDate.getReciever()+"|"+currentContributionDate.getTransactionDateString()+"|"+
						currentContributionDate.getMedianAmount()+"|" +currentContributionDate.getNoOfTransacations()+
						"|"+currentContributionDate.getTotalAmount() + "\n");
				
			} catch (IOException e) {
				System.out.println("Error while writing into output file - medianvals_by_zip");
				e.printStackTrace();
			}
		
	}
	
	
	/*
	 * Main method
	 */
	public static void main(String[] args) {
		//Read the file as it streams in and keep populating the data required for dashboard
		BufferedReader br = null;
		
		if (args.length == 3) {
			
			inputFile   = args[0];
			outputByZip = args[1];
			outputByDate = args[2];
			
			try {
				
				br = new BufferedReader(new FileReader(inputFile), bufferSize); 		
				String sCurrentLine;
				bwforZipFile  = new BufferedWriter(new FileWriter(outputByZip));
				bwforDateFile = new BufferedWriter(new FileWriter(outputByDate));
				
				while ((sCurrentLine = br.readLine()) != null) {

					sCurrentLine = sCurrentLine.trim();
					processRecord(sCurrentLine);
					
					//process contribution by zip
					if(checkValidityforZipcodeProcessing())
						processRecordForContributionByZip(cmteId, zipCode, transactionAmt);
					
					
					//process contribution by Date
					if(checkValidityforDateProcessing())
						processRecordForContributionByDate(cmteId, transactionDate, transactionAmt);
//					System.out.println(dateContributionDataMap.size());
					
				}
				
				priorityQueue = new PriorityQueue<>(queueComparator);
				//Sort and print the records
				addIntoPriorityQueue();
				writePriorityQueue();

			} catch (IOException e) {
				System.out.println("Error while reading the inpuit file - Check the path" + e.toString());
				e.printStackTrace();
			}
			
			finally {
				
					if(br != null) {
						try {
							br.close();
						} catch (IOException e) {
							System.out.println("Error when trying to close the file Reader " + e.toString());
							e.printStackTrace();
						}
					}
					
					if(bwforZipFile != null) {
							try {
								bwforZipFile.close();
							} catch (IOException e) {
								System.out.println("Error when trying to close the ZipFile Writer " + e.toString());
								e.printStackTrace();
							}
					}
					
					if(bwforDateFile != null) {
						try {
							bwforDateFile.close();
						} catch (IOException e) {
							System.out.println("Error when trying to close the DateFile Writer " + e.toString());
							e.printStackTrace();
						}
				}
			}
		} 
		else {
			System.out.println("Provide Valid arguments in Command line " + 
					"Java GenerateContributionData <Input-Path> <Output-path for Zip file> <Output-path for Date file>");
		}

		
		
	}

}
