import java.util.Comparator;

class ReciverandDateComparator implements Comparator<ContributionByDate> {

	@Override
	public int compare(ContributionByDate obj1, ContributionByDate obj2) {
		if(obj1.getReciever().equals(obj2.getReciever())) {
			return obj1.getTransactionDate().compareTo(obj2.getTransactionDate());
		}	
		else
			return obj1.getReciever().compareTo(obj2.getReciever());
	}

}
