import java.util.Collections;
import java.util.PriorityQueue;

class ContributionByZip {
	
	private String reciever;
	private String zipCode;
	private long medianAmount;
	private int noOfTransactions;
	private long totalAmount;
	private PriorityQueue<Long> minHeapLargerSet; //Stores the larger set in ascending order.
	private PriorityQueue<Long> maxHeapSmallerSet; //Stores the smaller set in reverse order.
	
	/*
	 * Initialize
	 */
	
	public ContributionByZip(String reciever, String zipCode, long totalAmount) {
		this.reciever = reciever;
		this.zipCode = zipCode;
		this.medianAmount = totalAmount;
		this.noOfTransactions = 1;
		this.totalAmount = totalAmount;
		this.minHeapLargerSet = new PriorityQueue<>();
		this.maxHeapSmallerSet = new PriorityQueue<>(Collections.reverseOrder()); 
		
		//Initially add the element into minSet
		this.minHeapLargerSet.offer(totalAmount);
	}
	
	/*
	 * Getters
	 */
	
	public String getReciever() {
		return this.reciever;
	}
	
	public String getZipcode() {
		return this.zipCode;
	}
	
	public String getMedianAmount() {
		return String.valueOf(this.medianAmount).split("\\.")[0];
	}
	
	public int getNoOfTransacations() {
		return this.noOfTransactions;
	}
	
	public String getTotalAmount() {
		return String.valueOf(this.totalAmount).split("\\.")[0];
	}
	
	public void updateNoOfTransactions() {
		this.noOfTransactions += 1;
	}
	
	/*
	 * Update the data for an existing transaction.
	 * -> Update the Total amount, Transaction count and the median gets modified accordingly.
	 */
	public void updateTotalAmount(long amount) {
		this.totalAmount += amount;
		this.updateNoOfTransactions();
		this.medianAmount = this.calculateMedian(amount, this.getNoOfTransacations());
	}	
	
	public long calculateMedian(long amount, int transactions) {
		
		
		if(minHeapLargerSet.size() != maxHeapSmallerSet.size()) {		
			if(minHeapLargerSet.size() > maxHeapSmallerSet.size()) {
				if(amount > minHeapLargerSet.peek()) {
					maxHeapSmallerSet.offer(minHeapLargerSet.poll());
					minHeapLargerSet.offer(amount);
				}else maxHeapSmallerSet.offer(amount);
			} 
			else if(maxHeapSmallerSet.size() > minHeapLargerSet.size()){
				if(amount < maxHeapSmallerSet.peek()) {
					minHeapLargerSet.offer(maxHeapSmallerSet.poll());
					maxHeapSmallerSet.offer(amount);
				}else minHeapLargerSet.offer(amount);
			}
		}
		else {
			if(amount < minHeapLargerSet.peek()) maxHeapSmallerSet.offer(amount);
			else minHeapLargerSet.offer(amount);
		}
		
	
		if ((minHeapLargerSet.size()) > (maxHeapSmallerSet.size())) return minHeapLargerSet.peek();
    		if ((minHeapLargerSet.size()) < (maxHeapSmallerSet.size())) return maxHeapSmallerSet.peek();
    		else return (long) (Math.round(((double)minHeapLargerSet.peek() + maxHeapSmallerSet.peek()) / 2.0));
		
		
	}

}
