#Extract contribution data 
##Receiver/zipcode - median_by_zip 
##Receiver/TransacationDate - median_by_date

** Program description **

GenerateContributionData - Main program for parsing the input file and basic validations, It stores the collections of objects containing ContributionByDate and ContributionByZip.


** Running Median and counts **

Every object for ContributionByDate, ContributionByZip is based
on unique combination of Receiver/TransactionDate and Receiver/zipcode respectively.


** Input file: **

To be placed in ./input folder - Sample file itcont.txt has been placed.


** Output file: **

Output files are emitted to ./output folder - By default the output files are medianvals_by_date.txt and medianvals_by_zip.txt


** Compiling and running the program: **

The compiltion and run command have been embedded in run.sh file given in the root path. 

- Compilation: Javac ./src/com/insight/contribution/*.java
- Running Program    : Java -cp ./src GenerateContributionData <input-file> <output-file1> <output-file2>
-  Example    : Java -cp ./src/ GenerateContributionData ./input/itcont.txt ./output/medianvals_by_zip.txt 
				./output/medianvals_by_date.txt


** Sample test cases and validation: **

The folder insight_testsuite/tests/test_2/input contains a random sampling of input file and validation of results.

