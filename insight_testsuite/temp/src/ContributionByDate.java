import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.PriorityQueue;

class ContributionByDate {
	
	private String reciever;
	private String transactionDate;
	private long medianAmount;
	private int noOfTransactions;
	private long totalAmount;
	private PriorityQueue<Long> minHeapLargerSet; //Stores the larger set in ascending order.
	private PriorityQueue<Long> maxHeapSmallerSet; //Stores the smaller set in reverse order.
	
	/*
	 * Inital object
	 */
	
	public ContributionByDate(String reciever, String transactionDate, long totalAmount) {
		this.reciever = reciever;
		this.transactionDate = transactionDate;
		this.medianAmount = totalAmount;
		this.noOfTransactions = 1;
		this.totalAmount = totalAmount;
		this.minHeapLargerSet = new PriorityQueue<>();
		this.maxHeapSmallerSet = new PriorityQueue<>(Collections.reverseOrder()); 
		
		//Initially add the element into minSet
		this.minHeapLargerSet.offer(totalAmount);
	}
	
	/*
	 * Getters
	 */
	
	public String getReciever() {
		return this.reciever;
	}
	
	public String getTransactionDateString() {
		return this.transactionDate;
	}
	
	public Calendar getTransactionDate() {
		
		DateFormat dateFormat = new SimpleDateFormat("MMddyyyy");
		Date date = null;
		Calendar cal = Calendar.getInstance();
		try {		
			date = dateFormat.parse(this.transactionDate);
			cal.setTime(date);
			cal.set(Calendar.MILLISECOND, 0);
			cal.set(Calendar.SECOND, 0);
			
		} catch (ParseException e) {
			System.out.println("Error processing the date" + e.toString());
			e.printStackTrace();
		}
		
		return cal;
	}
	
	public String getMedianAmount() {
		return String.valueOf(this.medianAmount).split("\\.")[0];
	}
	
	public int getNoOfTransacations() {
		return this.noOfTransactions;
	}
	
	public String getTotalAmount() {
		return String.valueOf(this.totalAmount).split("\\.")[0];
	}
	
	public void updateNoOfTransactions() {
		this.noOfTransactions += 1;
	}
	
	/*
	 * Update the data for an existing transaction.
	 * -> Update the Total amount, Transaction count and the median gets modified accordingly.
	 */
	public void updateTotalAmount(long amount) {
		this.totalAmount += amount;
		this.updateNoOfTransactions();
		this.medianAmount = this.calculateMedian(amount, this.getNoOfTransacations());
	}
	
	public long calculateMedian(long amount, int transactions) {
		
		if(minHeapLargerSet.size() != maxHeapSmallerSet.size()) {		
			if(minHeapLargerSet.size() > maxHeapSmallerSet.size()) {
				if(amount > minHeapLargerSet.peek()) {
					maxHeapSmallerSet.offer(minHeapLargerSet.poll());
					minHeapLargerSet.offer(amount);
				}else maxHeapSmallerSet.offer(amount);
			} 
			else if(maxHeapSmallerSet.size() > minHeapLargerSet.size()){
				if(amount < maxHeapSmallerSet.peek()) {
					minHeapLargerSet.offer(maxHeapSmallerSet.poll());
					maxHeapSmallerSet.offer(amount);
				}else minHeapLargerSet.offer(amount);
			}
		}
		else {
			if(amount < minHeapLargerSet.peek()) maxHeapSmallerSet.offer(amount);
			else minHeapLargerSet.offer(amount);
		}
		
	
		if ((minHeapLargerSet.size()) > (maxHeapSmallerSet.size())) return minHeapLargerSet.peek();
    		if ((minHeapLargerSet.size()) < (maxHeapSmallerSet.size())) return maxHeapSmallerSet.peek();
    		else return (long) (Math.round(((double)minHeapLargerSet.peek() + maxHeapSmallerSet.peek()) / 2.0));
			
		
	}

}
